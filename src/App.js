import Navbar from './components/Navbar/Navbar';
import Admin from './views/admin/Admin'

import {
  Switch,
  BrowserRouter, Route, Router
} from "react-router-dom";
import Portal from './views/portal/Portal';
import history from './config/history'


function App() {
  return (
    <BrowserRouter >
          <Navbar></Navbar>
            <main>
              <Router history={history}>
              <Switch> 
                <Route component={Admin} path="/admin" />
                <Route component={Portal} path="/" />
              </Switch>
              </Router>
            </main>
    </BrowserRouter>
  );
}

export default App;
