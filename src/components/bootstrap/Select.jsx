import { Form } from 'react-bootstrap';
import { selectChampionship } from '../../store/championshipSelected';
import { useDispatch } from 'react-redux';
import editChampionship from '../../store/editChampionship';
const Select = (props) => {
    const dispatch = useDispatch()
    function handleSelect(id) {
        dispatch(selectChampionship(id))
    }

    function handleChange(e) {

        var index = e.target.selectedIndex;
        var optionElement = e.target.childNodes[index]
        console.log(optionElement)
        handleSelect(optionElement.getAttribute('_id'))
    }

    return (
        <>
            <Form.Group controlId="exampleForm.ControlSelect1">
                <Form.Label>Selecione um campeonato</Form.Label>
                <Form.Control id='select' as="select" onChange=
                    {(event) => handleChange(event)}>
                    <option _id={null}>Selecione um Campeonato</option>
                    {props.options.map((championship) => (
                        <option _id={championship._id}>{championship.name}</option>
                    ))}
                </Form.Control>
            </Form.Group>
        </>
    )
}

export default Select
