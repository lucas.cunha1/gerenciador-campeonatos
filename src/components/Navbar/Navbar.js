import React, { useEffect } from 'react'
import { Navbar as NavbarBootstrap, NavDropdown, Nav } from 'react-bootstrap';
import { removeToken, getUser } from '../../config/auth';
import { useDispatch, useSelector } from 'react-redux';
import { loginStore, logoutStore } from '../../store/user';
import history from '../../config/history';
import NavbarUnlogged from './NavbarUnlogged';
import { Fragment } from 'react';
import NavbarAdmin from './NavbarAdmin';
import NavbarUser from './NavbarUser';
const Navbar = (props) => {
    const dispatch = useDispatch()
    const loginState = useSelector((state) => state.user)

    useEffect(() => {
        const user = getUser()
        if (user) {
            dispatch(loginStore(user))
        }
        return () => { };
    }, [])

    function logout() {
        removeToken()
        dispatch(logoutStore())
        history.push('/')
    }
    function goToHome() {
        if (loginState.isAdmin === true) {
            history.push('/admin/homeAdmin')
        } else {
            history.push('/home')
        }
    }
    return (
        <>

            <header>
                <NavbarBootstrap bg="success" variant='dark' expand="lg">
                    <div className='container'>
                        <NavbarBootstrap.Brand>Gerenciador de Campeonatos</NavbarBootstrap.Brand>
                        <NavbarBootstrap.Toggle aria-controls="basic-navbar-nav" />
                        <NavbarBootstrap.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                {loginState.userId === null ?
                                    <NavbarUnlogged /> : loginState.isAdmin === true ?
                                        <NavbarAdmin logout={logout}
                                            goToHome={goToHome} userName={loginState.name} />
                                        : <NavbarUser logout={logout}
                                            goToHome={goToHome} userName={loginState.name} />}

                            </Nav>
                        </NavbarBootstrap.Collapse>
                    </div>
                </NavbarBootstrap>
            </header>

        </>
    )
}

export default Navbar
