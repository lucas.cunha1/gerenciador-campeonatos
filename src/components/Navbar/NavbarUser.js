import React from 'react'
import { navigateToChampionshipList, navigateToCreateTeam, navigateToMyTeams } from '../../views/portal/Home'
import { Nav, NavDropdown } from 'react-bootstrap';

const NavbarUser = (props) => {
  return (
    <>
      <Nav.Link onClick={props.goToHome}>Home</Nav.Link>
            <NavDropdown title={props.userName} id="basic-nav-dropdown">
                <NavDropdown.Item onClick={navigateToMyTeams}>Meus Times</NavDropdown.Item>
                <NavDropdown.Item onClick={navigateToCreateTeam}>Criar Times</NavDropdown.Item>
                <NavDropdown.Item onClick={navigateToChampionshipList}>Campeonatos</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item onClick={props.logout}>Sair</NavDropdown.Item>
            </NavDropdown>
    </>
  )
}

export default NavbarUser
