import React from 'react'
import history from '../../config/history';
import { Nav } from 'react-bootstrap';

const NavbarUnlogged = () => {
    return (
        <>
            <Nav.Link onClick={() => history.push('/login')}>Login</Nav.Link>
            <Nav.Link onClick={() => history.push('/cadastrar')}>Cadastre-se</Nav.Link>
        </>
    )
}

export default NavbarUnlogged
