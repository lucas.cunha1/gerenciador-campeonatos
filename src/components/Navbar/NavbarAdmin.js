import React from 'react'
import { NavDropdown, Nav } from 'react-bootstrap';
import {
    navigateToCadastrarCampeonatos,
    navigateToChampionshipList,
    navigateToTeams,
    navigateToTeamsPendingList
} from '../../views/admin/championship/HomeAdmin';

const NavbarAdmin = (props) => {
    
    return (
        <>
            <Nav.Link onClick={props.goToHome}>Home</Nav.Link>
            <NavDropdown title={props.userName} id="basic-nav-dropdown">
            <NavDropdown.Item onClick={navigateToCadastrarCampeonatos}>Cadastrar Campeonatos</NavDropdown.Item>
                <NavDropdown.Item onClick={navigateToChampionshipList}>Listar Campeonatos</NavDropdown.Item>
                <NavDropdown.Item onClick={navigateToTeams}>Times</NavDropdown.Item>
                <NavDropdown.Item onClick={navigateToTeamsPendingList}>Times Pendentes</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item onClick={props.logout}>Sair</NavDropdown.Item>
            </NavDropdown>
        </>
    )
}

export default NavbarAdmin
