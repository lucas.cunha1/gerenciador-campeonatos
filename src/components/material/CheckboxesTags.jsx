import React, {useState} from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { add, remove } from '../../store/championshipSelected';
import { useDispatch } from 'react-redux';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export default function CheckboxesTags(props) {

  const dispatch = useDispatch()
  
  function handleCheckbox(event){
    // if(event.target.checked === true){
    //   dispatch(add(event.target.value))
    // }else{
    //   dispatch(remove(event.target.value))
    // }
  }

  return (
    <Autocomplete 
      multiple
      id="checkboxes-tags-demo"
      options={props.options}
      disableCloseOnSelect={true}
      onChange={(event) => handleCheckbox(event)}
      getOptionLabel={(option) => option.name}
      renderOption={(option, { selected }) => (
        <div>
          <Checkbox
            className={props.checkboxClassName}
            icon={icon}
            checkedIcon={checkedIcon}
            style={{ marginRight: 8 }}
            checked={selected}
            value={option._id}
          />
          <span value={option._id} id='championshipId'>{option.name}</span>
        </div>
      )}
      style={{ width: 500 }}
      renderInput={(params) => (
        <TextField {...params} variant="outlined" label={props.label}/>
      )}
    />
  );
}
