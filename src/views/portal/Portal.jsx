import {
  Redirect,
  Route,
} from "react-router-dom";
import Login from './Login';
import Team from './Team';
import Cadastrar from './Cadastrar';
import Home from './Home';
import ChampionshipList from './ChampionshipList';
import ChampionshipDetail from "./ChampionshipDetail";
import MyTeams from './MyTeams';
import MyTeamDetail from './MyTeamDetail';
import history from '../../config/history';
import { useSelector } from 'react-redux';

const Portal = (props) => {
  const loginState = useSelector((state) => state.user)

  const UserRoute = ({...rest}) => {
    if(!loginState.userId && history.location.pathname !== '/login' 
    && history.location.pathname !== '/cadastrar'){
      return <Redirect push to="/login" />
    }
    return <Route {...rest}></Route>
    
  }

  return (
    <>
      <Route exact basename={props.match.path} path={props.match.path + '/'} component={Login} />
      <Route exact basename={props.match.path} path={props.match.path + 'login'} component={Login} />
      <Route exact basename={props.match.path} path={props.match.path + 'cadastrar'} component={Cadastrar} />
      <UserRoute exact basename={props.match.path} path={props.match.path + 'team'} component={Team} />
      <UserRoute exact basename={props.match.path} path={props.match.path + 'home'} component={Home} />
      <UserRoute exact basename={props.match.path} path={props.match.path + 'championships'} component={ChampionshipList} />
      <UserRoute exact basename={props.match.path} path={props.match.path + 'championships/detail'} component={ChampionshipDetail} />
      <UserRoute exact basename={props.match.path} path={props.match.path + 'myTeams'} component={MyTeams} />
      <UserRoute exact basename={props.match.path} path={props.match.path + 'myTeams/detail'} component={MyTeamDetail} />
      <UserRoute exact basename={props.match.path} path={props.match.path + 'myTeams/team/edit'} component={() => <Team edit={true}></Team>} />

    </>
  )
}

export default Portal
