import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { getUser } from '../../config/auth';
import { fetchUser } from '../../store/getUser';
import { ListGroup } from 'react-bootstrap';
import history from '../../config/history';
import { selectTeam } from '../../store/myTeamDetailSelected';
import { CenterDiv } from './ChampionshipList';
import { CircularProgress } from '@material-ui/core';

const MyTeams = () => {
    const dispatch = useDispatch()
    const [teams, setTeams] = useState([])
    const [loading, setLoading] = useState(true)
    useEffect(async () => {
        setLoading(true)
        let user = getUser()
        let data = await dispatch(fetchUser(user.id))
        setTeams(data.payload.posted_teams)
        setLoading(false)
        return () => {
        };
    }, [])

    function navigateToMyTeamDetail(team){
        dispatch(selectTeam(team))
        history.push('/myTeams/detail')
    }

    function mountTeamList(){
        return teams.map((team) => (
            <ListGroup.Item
                key={team._id} onClick={() => navigateToMyTeamDetail({id: team._id, name: team.name})} className='item'>
                {team.name}</ListGroup.Item>
        ))
    }

    return (
        <>
            <div class='container'>
                <h2 className='mt-3'>Meus Times</h2>
                {loading ? <CenterDiv>
                    <CircularProgress size={65}></CircularProgress>
                </CenterDiv> : 
                <ListGroupStyled className='mt-4 mb-4'>
                    {mountTeamList()}
                </ListGroupStyled>} 
            </div >
        </>
    )
}

export default MyTeams

const ListGroupStyled = styled(ListGroup)`
    .item:hover{
        cursor: pointer;
        color: #0000EE;
    }
`
