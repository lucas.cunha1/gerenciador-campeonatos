import { useEffect, useState } from 'react'
import { Button, Form, Spinner } from 'react-bootstrap';
import styled from 'styled-components';
import CheckboxesTags from '../../components/material/CheckboxesTags';
import { useDispatch, useSelector } from 'react-redux'
import { fetchChampionship } from '../../store/championships';
import { createTeamAndAddChampionship, resetLoaded, fetchPatchTeam, loaded } from '../../store/teamPost';
import { getUser } from '../../config/auth';
import history from '../../config/history';
import championshipSelected, { selectChampionship } from '../../store/championshipSelected';
import { InputLabel, MenuItem } from '@material-ui/core';
import { Select } from '@material-ui/core';
import { useAlert } from 'react-alert'
const Team = (props) => {

    const state = useSelector(state => state.championshipReducer)
    const nomeTimeEdit = useSelector(state => state.myTeamDetailSelected.name)
    const teamId = useSelector(state => state.myTeamDetailSelected.id)
    const createTeamState = useSelector(state => state.createTeam)
    const dispatch = useDispatch()
    const [nomeTime, setNomeTIme] = useState('')
    const championshipSelectedState = ((state) => state.championshipSelected)
    const [championship, setChampionship] = useState('')
    const alert = useAlert()
    

    useEffect(() => {
        dispatch(fetchChampionship())
    }, [dispatch, fetchChampionship])

    useEffect(() => {
        if (props.edit) {
            setNomeTIme(nomeTimeEdit)
        }
        return () => {

        };
    }, [])

    if (createTeamState.editTeamAddChampionship.loaded) {
        dispatch(resetLoaded())
        history.push('/home')
    }


    async function handleSubmit() {
        const user = getUser()
        const response = await dispatch(createTeamAndAddChampionship({
            nome: nomeTime, user_id: user.id,
            championshipId: championship
        }))
    }

    async function handleEditSubmit() {
        const response = await dispatch(fetchPatchTeam({
            teamId: teamId,
            championshipId: championship
        }))
        if(response.payload === 'Request failed with status code 412'){
            alert.show(<div style={{ fontSize: '1.8rem' }}>Esse time já está cadastrado neste campeonato!</div>, {
                title: "Erro!",
        
                timeout: '5000'
        
              });

        }else{
            dispatch(loaded())
        }
    }

    function handleChangeNomeTime(event) {
        event.preventDefault()
        setNomeTIme(event.target.value)
    }

    function handleSelectChange(event) {
        dispatch(selectChampionship(event.target.value))
        setChampionship(event.target.value)
    }

    return (
        <>
            <DivForm className='container'>
                <Form>
                    <h2 className='mb-3'>{props.edit ? 'Adicionar campeonato' : 'Criar Time'}</h2>
                    <Form.Group id='teamName' controlId='formBasicTeam'>
                        <Form.Label>Nome do time</Form.Label>
                        <Form.Control value={nomeTime} onChange={(event) => handleChangeNomeTime(event)} type="text" placeholder="" />
                    </Form.Group>
                    <InputLabel for='championship-select'>Inscreva seu time em um torneio.</InputLabel>
                    <Select fullWidth={true}
                        labelId="championship-select"
                        id="championship-select"
                        value={championship}
                        onChange={(event) => handleSelectChange(event)}
                    >
                        {state.data ? state.data.map((item) => (
                            <MenuItem value={item._id}>{item.name}</MenuItem>
                        )) : ''}

                    </Select>
                    <Form.Text className="text-muted">
                        Sujeito a aprovação.
                    </Form.Text>
                    {createTeamState.createTeam.loading || createTeamState.editTeamAddChampionship.loading
                        ? <div className='div-spinner'><Spinner variant='primary' className='spinner' animation="border" /></div> : ''}
                    <Button disabled={!nomeTime} onClick={props.edit ? handleEditSubmit : handleSubmit} className='mt-3'>{props.edit ? 'Editar' : 'Criar'}</Button>
                </Form>
            </DivForm>
        </>
    )
}

const DivForm = styled.div`
    padding: 2rem 1rem 1rem 2rem;
    .div-spinner{
        margin-top: 1rem;
        margin-left: 0.5rem;
    }
    .spinner{
        display: block;
    }
`

export default Team
