import React, { useEffect, useState } from 'react'
import { ListGroup, Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { fetchChampionship } from '../../store/championships';
import history from '../../config/history';
import { selectChampionship } from '../../store/championshipDetailSelected';
import { Fragment } from 'react';
import { CircularProgress } from '@material-ui/core';

const ChampionshipList = () => {
    const state = useSelector(state => state.championshipReducer)
    const [championships, setChampionships] = useState([])
    const [loading, setLoading] = useState(true)
    const dispatch = useDispatch()
    useEffect(async () => {
        setLoading(true) 
        let data = await dispatch(fetchChampionship())
        setChampionships(data.payload)
        setLoading(false)
    }, [])

    function navigateToChampionshipPage(payload) {
        dispatch(selectChampionship(payload))
        history.push('/championships/detail')
    }

    let championshipType = {
        GROUP_STAGE: 'Fase de Grupos',
        PLAYOFFS: 'Mata-Mata',
        PLAYOFFSAndGROUP_STAGE: 'Fase de Grupos e Mata-Mata'
    }

    return (
        <>
            <div class='container'>
                <h2 className='mt-3'>Lista de Campeonatos</h2>
                {loading ? <CenterDiv>
                        <CircularProgress color='primary' size={65}></CircularProgress>
                    </CenterDiv> : <Table responsive striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nome do Campeonato</th>
                            <th>Tipo de Torneio</th>
                        </tr>
                    </thead>
                    <tbody>
                    {championships.map((championship) => (
                        <Fragment>
                        <TrStyled>
                        <td className='item' onClick={() => navigateToChampionshipPage({ id: championship._id, name: championship.name })}
                            key={championship._id} className='item'>
                            {championship.name}
                        </td>
                        <td>{championshipType[championship.phases]}
                        </td>
                        </TrStyled>
                        </Fragment>
                    ))}
                    </tbody>
                </Table>}
            </div >
        </>
    )
}

const TrStyled = styled.tr`
    .item:hover{
        cursor: pointer;
        color: #0000EE;
    }
`

export const CenterDiv = styled.div`
display: flex;
align-items:center;
justify-content:center;
    height: 12rem;
    width: 100%;
`
export default ChampionshipList
