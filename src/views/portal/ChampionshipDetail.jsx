import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { fetchChampionshipById } from '../../store/getChampionshipById';
import { ListGroup, Form } from 'react-bootstrap';
import styled from 'styled-components';
import { CenterDiv } from './ChampionshipList';
import { CircularProgress } from '@material-ui/core';
import { Fragment } from 'react';

const ChampionshipDetail = () => {
    const championship = useSelector(state => state.championshipDetailSelected)
    const dispatch = useDispatch()
    const [teams, setTeams] = useState([])
    const [loading, setLoading] = useState(true)
    const [existemTimes, setExistemTimes] = useState(false)
    useEffect(() => {
        (async()=>{
            const data = await dispatch(fetchChampionshipById(championship.id))
            setTeams(data.payload.teams)
            setLoading(false)
        })()
        return () => {};
    }, [])

    return (
        <>
            <div class='container'>
                <StyledH2 className='mt-3'>{championship.name}</StyledH2>
                <h3 className='mt-5'>Times Participantes</h3>
                {loading ? <CenterDiv>
                <CircularProgress size={65}></CircularProgress>
                </CenterDiv> : 
                <Fragment>
                <Form.Text className="text-muted">
                            Apenas times que foram aprovados pelo administrador.
                        </Form.Text>
                <ListGroupStyled className='mt-4 mb-4'>
                {teams.map((team) => {
            if (team.team_id && team.status === 'APPROVED') {
                if(!existemTimes){
                    setExistemTimes(true)
                }
                return (
                    <ListGroup.Item
                        key={team._id} className='item'>
                        {team.team_id.name}</ListGroup.Item>
                )
            }
        })}
                    {existemTimes ? '' : <p>Não há times para este campeonato.</p> }
                </ListGroupStyled>
                </Fragment>}
            </div >
        </>
    )
}

export default ChampionshipDetail

const ListGroupStyled = styled(ListGroup)`
    
`
const StyledH2 = styled.h2`
    overflow-wrap: break-word;
`
