import { useEffect, useState } from 'react'
import { Form, Button, Alert, Spinner } from 'react-bootstrap';
import styled from 'styled-components';
import { cadastro } from '../../services/user';
import { saveLocalStorage, getUser } from '../../config/auth';
import { useHistory } from 'react-router-dom';
import http from '../../config/config';
import { loginStore } from '../../store/user';
import { useDispatch } from 'react-redux';
const Cadastrar = () => {

    const [email, setEmail] = useState('')
    const [senha, setSenha] = useState('')
    const [nome, setNome] = useState('')
    const [loading, setLoading] = useState(false)
    const [mostrarAlertError, setMostrarAlertError] = useState(false)
    const [mensagensErro, setMensagensErro] = useState([])
    const history = useHistory()
    const dispatch = useDispatch()
    function formIsValid() {
        return email && senha
    }
    useEffect(() => {
        const user = getUser()
        if(user.id){
            if(user.is_admin){
                history.push('/admin/homeAdmin')
            }else{
                history.push('/home')
            }
        }
      return () => {};
    }, [])

    async function handleLogin() {
        setLoading(true)
        try {
            const response = await cadastro({name: nome, email, senha})
            console.log(response)
            const token = response.data.token
            saveLocalStorage({token: token, user: response.data.user})
            const {name, _id, is_admin} = response.data
            dispatch(loginStore({name, is_admin, id: _id }))
            http.defaults.headers['x-auth-token'] = token;
            history.push('/team')
        } catch (error) {
            let erros = [];
            error.response.data.errors.map((error, index) => {
                return erros.push(<li key={index} >{error.msg}</li>)
            })
            setLoading(false)
            setMensagensErro(erros)
            setMostrarAlertError(true)
        }
    }

    return (
        <>
            <DivForm>
                <Form className='container form'>
                <h1>Cadastro</h1>
                <Alert show={mostrarAlertError} variant="danger" onClose={() => setMostrarAlertError(false)} dismissible>
                    <Alert.Heading>{mensagensErro.length > 1 ? 'Erros:' : 'Ocorreu um Erro!'}</Alert.Heading>
                    <ul>
                    {mensagensErro}
                    </ul>
            </Alert>
            <Form.Group controlId="formBasic">
                        <Form.Label>Nome</Form.Label>
                        <Form.Control type="email" placeholder="" value={nome} onChange={(event) => setNome(event.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Endereço de Email</Form.Label>
                        <Form.Control type="email" placeholder="" value={email} onChange={(event) => setEmail(event.target.value)} />
                        <Form.Text className="text-muted">
                            Nós não compartilharemos seu email com ninguém
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Senha</Form.Label>
                        <Form.Control type="password" placeholder="" value={senha} onChange={(event) => setSenha(event.target.value)} />
                    </Form.Group>
                    <Button disabled={!formIsValid()} onClick={handleLogin} variant="primary" type="button">
                        Cadastre-se
                    </Button>
                    {loading ? <Spinner variant='primary' className='spinner' animation="border"/> : ''}
                </Form>
            </DivForm>
        </>
    )
}

const DivForm = styled.div`
    .form{
        padding: 1.5rem;
        max-width: 600px;
    }
    h1{
        padding-bottom: 1rem;
    }
    .spinner{
        margin-left: 1rem;
    }
`

export default Cadastrar
