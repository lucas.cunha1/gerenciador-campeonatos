import { Button } from "react-bootstrap"
import styled from "styled-components"
import history from '../../config/history';

const Home = () => {

  return (
    <>
      <div className='container'>
        <HomeContent>
          <Button className='button' onClick={navigateToChampionshipList}
          variant='outline-info'>Ver todos os campeonatos</Button>
          <Button className='button' onClick={navigateToCreateTeam} 
          variant='outline-info'>Criar novo time</Button>
          <Button className='button' onClick={navigateToMyTeams} 
          variant='outline-info'>Meus Times</Button>
        </HomeContent>
      </div>
    </>
  )
}

export default Home

const HomeContent = styled.div`
margin-top: 1.8rem;
  display: flex;
  .button{
    margin-right: 2rem;
  }
  @media screen and (max-width: 767px){
    width: 91vw;
    position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
    .button{
    width: 100%;
    margin-right: 0;
    margin-bottom: 1.2rem;
  }
    flex-direction: column;
  }
  
`

export function navigateToChampionshipList(){
  history.push('/championships')
}

export function navigateToCreateTeam(){
  history.push('/team')
}

export function navigateToMyTeams(){
  history.push('/myTeams')
}