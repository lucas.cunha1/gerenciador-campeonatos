import { useEffect, useState } from 'react'
import { Form, Button, Alert, Spinner } from 'react-bootstrap';
import styled from 'styled-components';
import { login } from '../../services/user';
import {loginStore} from '../../store/user.js'
import { saveToken, getToken, saveLocalStorage, getUser } from '../../config/auth';
import { useHistory } from 'react-router-dom';
import http from '../../config/config';
import { useDispatch } from 'react-redux';
import history from '../../config/history';

const Login = () => {

    const [email, setEmail] = useState('')
    const [senha, setSenha] = useState('')
    const [loading, setLoading] = useState(false)
    const [mostrarAlertError, setMostrarAlertError] = useState(false)
    const [mensagensErro, setMensagensErro] = useState([])
    const history = useHistory()
    const dispatch = useDispatch()

    useEffect(() => {
        const user = getUser()
        if(user.id){
            if(user.is_admin){
                history.push('/admin/homeAdmin')
            }else{
                history.push('/home')
            }
        }
      return () => {};
    }, [])

    function formIsValid() {
        return email && senha
    }

    async function handleLogin() {
        setLoading(true)
        try {
            const response = await login(email, senha)
            saveLocalStorage({token: response.data.token, user: response.data.user})
            dispatch(loginStore(response.data.user))
            http.defaults.headers['x-auth-token'] = getToken();
            if(response.data.user.is_admin === false){
                history.push('/home')
            }else{
                history.push('/admin/homeAdmin')
            }
        } catch (error) {
            let erros = [];
            error.response.data.errors.map((error, index) => {
                return erros.push(<li key={index} >{error.msg}</li>)
            })
            setLoading(false)
            setMensagensErro(erros)
            setMostrarAlertError(true)
        }
    }

    return (
        <>
            <DivForm>
                <Form className='container form'>
                <h1>Login</h1>
                <Alert show={mostrarAlertError} variant="danger" onClose={() => setMostrarAlertError(false)} dismissible>
                    <Alert.Heading>{mensagensErro.length > 1 ? 'Erros:' : 'Ocorreu um Erro!'}</Alert.Heading>
                    <ul>
                    {mensagensErro}
                    </ul>
            </Alert>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Endereço de Email</Form.Label>
                        <Form.Control type="email" placeholder="" value={email} onChange={(event) => setEmail(event.target.value)} />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Senha</Form.Label>
                        <Form.Control type="password" placeholder="" value={senha} onChange={(event) => setSenha(event.target.value)} />
                    </Form.Group>
                    <Button disabled={!formIsValid()} onClick={handleLogin} variant="primary" type="button">
                        Entrar
                    </Button>
                    {loading ? <Spinner variant='primary' className='spinner' animation="border"/> : ''}
                </Form>
            </DivForm>
        </>
    )
}

const DivForm = styled.div`
    .form{
        padding: 1.5rem;
        max-width: 600px;
    }
    h1{
        padding-bottom: 1rem;
    }
    .spinner{
        margin-left: 1rem;
    }
`

export default Login
