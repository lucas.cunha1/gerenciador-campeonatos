import React, { useEffect, useState } from 'react'
import { Table, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import { fetchTeamById } from '../../store/getTeamById';
import history from '../../config/history';
import { statusChampionship } from '../admin/championship/TeamDetail';
import { CenterDiv } from './ChampionshipList';
import { CircularProgress } from '@material-ui/core';
import { Fragment } from 'react';

const MyTeamDetail = (props) => {

    const teamDetail = useSelector(state => state.myTeamDetailSelected)
    const dispatch = useDispatch()
    const [championships, setChampionships] = useState([])
    const [existemCampeonatos, setExistemCampeonatos] = useState(true)
    const [loading, setLoading] = useState(true)
    useEffect(async () => {
        setLoading(true)
        const data = await dispatch(fetchTeamById(teamDetail.id))
        let arrChamp = []
        for (const championship of data.payload.championship) {
            if(championship?.championship_id?._id){
                arrChamp.push(championship)
            }
        }
        setChampionships(arrChamp)
        setLoading(false)
        return () => {

        };
    }, [dispatch, fetchTeamById])

    useEffect(() => {
        if(!teamDetail.name){
            history.push('/home')
        }
      return () => {
        
      };
    }, [])

    function mountTable() {
        if( championships.length > 0){
            return championships.map((championship) => {
                    if(!existemCampeonatos){
                        setExistemCampeonatos(true)
                    }
                    return (
                        <tr key={championship.championship_id?._id}>
                            <td>{championship.championship_id?.name}</td>
                            <td>{statusChampionship[championship.status]}</td>
                        </tr>
                    )
                
            })
        }else{
            setExistemCampeonatos(false)
        }
    }

    function navigateToPatchTeam() {
        props.history.push('team/edit')
    }

    return (
        <>
            <div class='container'>
                <h2>{teamDetail.name}</h2>
                {loading ? 
                <CenterDiv>
                    <CircularProgress size={65}></CircularProgress>
                </CenterDiv> : 
                <Fragment>
                {existemCampeonatos ?   
                <Table responsive striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nome do Campeonato</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {mountTable()}
                    </tbody>
                </Table>
                : <p>Esse time não está inscrito em nenhum campeonato.</p>}
                <Button onClick={navigateToPatchTeam}>Adicionar campeonatos ao Time {teamDetail.name}</Button>
                </Fragment>}
                
            </div>
        </>
    )
}

export default MyTeamDetail
