import { Button } from "react-bootstrap"
import styled from "styled-components"
import history from '../../../config/history';

const HomeAdmin = () => {

  return (
    <>
      <div className='container'>
        <HomeContent>
        <Button className='button' onClick={navigateToCadastrarCampeonatos}
          variant='outline-info'>Cadastrar campeonatos</Button>
          <Button className='button' onClick={navigateToChampionshipList}
          variant='outline-info'>Ver todos os campeonatos</Button>
          <Button className='button' onClick={navigateToTeams} 
          variant='outline-info'>Ver Todos os Times</Button>
          <Button className='button' onClick={navigateToTeamsPendingList} 
          variant='outline-info'>Ver Times Pendentes de Aprovação</Button>
        </HomeContent>
      </div>
    </>
  )
}

export default HomeAdmin

const HomeContent = styled.div`
margin-top: 1.8rem;
  display: flex;
  .button{
    margin-right: 2rem;
  }
  @media screen and (max-width: 767px){
    width: 91vw;
    position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
    .button{
    width: 100%;
    margin-right: 0;
    margin-bottom: 1.2rem;
  }
    flex-direction: column;
  }
  
`

export function navigateToChampionshipList(){
  history.push('/admin/championshipList')
}

export function navigateToTeams(){
  history.push('/admin/teamsList')
}

export function navigateToTeamsPendingList(){
  history.push('/admin/teamsPending')
}

export function navigateToCadastrarCampeonatos(){
  history.push('/admin/championship')
}