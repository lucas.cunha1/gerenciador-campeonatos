import React, { useEffect, useState } from 'react'
import { Table, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import history from '../../../config/history';
import { fetchTeamById } from '../../../store/getTeamById';
import { Tooltip, Button as MaterialButton, CircularProgress } from '@material-ui/core';
import styled from 'styled-components';
import { fetchEditarStatusTime } from '../../../store/editarStatusTime';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import { Fragment } from 'react';
import { CenterDiv } from '../../portal/ChampionshipList';


const TeamDetail = (props) => {

    const teamDetail = useSelector(state => state.myTeamDetailSelected)
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(true)
    const [championships, setChampionships] = useState([])
    const [attPage, setAttPage] = useState(false)

    useEffect(async () => {
        const data = await dispatch(fetchTeamById(teamDetail.id))
        let arrChamps = []
        for (const championship of data.payload.championship) {
            if(championship?.championship_id?._id){
                arrChamps.push(championship)
            }
        }
        setChampionships(arrChamps)
        setLoading(false)
        return () => {};
    }, [dispatch, fetchTeamById, attPage])

    useEffect(() => {
        if(!teamDetail.name){
            history.push('/home')
        }
      return () => {};
    }, [])

    async function handleClickDisapprove(championshipId){
        const response = await dispatch(fetchEditarStatusTime({teamId: teamDetail.id, championshipId, status: 'DISAPPROVED'}))
        if(response.payload.status){
            setAttPage(!attPage)
        } 
    }
    async function handleClickApprove(championshipId){
        const response = await dispatch(fetchEditarStatusTime({teamId: teamDetail.id, championshipId, status: 'APPROVED'}))
        if(response.payload.status){
            setAttPage(!attPage)
        } 
    }

    function mountTable() {
        
        if(championships.length > 0){
            return championships.map((championship) => {
            
                return (
                    <tr>
                        <td>{championship.championship_id.name}</td>
                        <td>{statusChampionship[`${championship.status}`]}</td>
                        <td style={{maxWidth: "10px"}}>
                            <Tooltip title="Aprovar este time.">
                                <MaterialButton onClick={() => handleClickApprove( 
                                championship.championship_id._id)} size="small">
                                    <CheckIconGreen></CheckIconGreen>
                                </MaterialButton>
                            </Tooltip>
                            <Tooltip title="Rejeitar este time.">
                                <MaterialButton onClick={() => 
                                handleClickDisapprove(championship.championship_id._id)} 
                                size="small">
                                    <CloseIconRed></CloseIconRed>
                                </MaterialButton>
                            </Tooltip>
                        </td>
                    </tr>
                )
            
        })
        }else{
            return <p>Este time não está inscrito em nenhum campeonato.</p>
        }
    }

    function navigateToPatchTeam() {
        props.history.push('team/edit')
    }

    return (
        <>
            <div class='container'>
                <h2>{teamDetail.name}</h2>
                {loading ? 
                <CenterDiv>
                    <CircularProgress size={65}></CircularProgress>
                </CenterDiv> :
                <Fragment>
                <Table responsive striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nome do Campeonato</th>
                            <th>Status</th>
                            <th>Aprovar?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {mountTable()}
                    </tbody>
                </Table>
                <Button onClick={navigateToPatchTeam}>Adicionar campeonatos ao 
                Time {teamDetail.name}</Button>
                </Fragment>} 
            </div>
        </>
    )
}

export default TeamDetail

export const statusChampionship = {
    PENDING: 'Pendente',
    APPROVED: 'Confirmado',
    DISAPPROVED: 'Rejeitado'
}

const CheckIconGreen = styled(CheckIcon)`
    color: green;
`
const CloseIconRed = styled(CloseIcon)`
    color: red;
`