import React, {useState, useEffect} from 'react'
import { Table } from 'react-bootstrap';
import { fetchListarTimesPendentes } from '../../../store/listarTimesPendentes';
import { useDispatch } from 'react-redux';
import { Fragment } from 'react';
import history from '../../../config/history';
import { selectTeam } from '../../../store/myTeamDetailSelected';
import { statusChampionship } from './TeamDetail';
import { fetchEditarStatusTime } from '../../../store/editarStatusTime';
import { Tooltip, Button as MaterialButton, CircularProgress } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import styled from 'styled-components';
import { CenterDiv } from '../../portal/ChampionshipList';
const TeamPendingList = () => {
    const [teams, setTeams] = useState([])
    const [attPage, setAttPage] = useState(false)
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        (async()=>{
            const response = await dispatch(fetchListarTimesPendentes())
            const arr = []
            for (const team of response.payload.pending) {
                if(team.team_id && team.championship_id){
                    arr.push(team)
                }
            }
            setTeams(arr)
            setLoading(false)
        })()
        return () => { };
    }, [attPage])

    function navigateToTeamPage(name, id){
        dispatch(selectTeam({name, id}))
        history.push('/admin/teamsList/detail')
    }
    async function handleClickDisapprove(championshipId, teamId){
        const response = await dispatch(fetchEditarStatusTime({teamId, championshipId, status: 'DISAPPROVED'}))
        if(response.payload.status){
            setAttPage(!attPage)
        } 
    }
    async function handleClickApprove(championshipId, teamId){
        const response = await dispatch(fetchEditarStatusTime({teamId, championshipId, status: 'APPROVED'}))
        if(response.payload.status){
            setAttPage(!attPage)
        } 
    }

    function mountTeams() {

        console.log(teams)
        if (teams.length > 0) {
            return teams.map((team) => {
                    console.log(team)
                    return (
                        <Fragment>
                            <tr>
                                <td className='item' onClick={() => navigateToTeamPage(team.team_id.name, team.team_id._id)}
                                    key={team.team_id._id} className='item'>
                                    {team.team_id.name}
                                </td>
                                <td>
                                    {team.championship_id?.name}
                                </td>
                                <td>{statusChampionship[team.status]}
                                </td>
                                <td style={{maxWidth: "10px"}}>
                            <Tooltip title="Aprovar este time.">
                                <MaterialButton onClick={() => handleClickApprove( 
                                team.championship_id._id, team.team_id._id)} size="small">
                                    <CheckIconGreen></CheckIconGreen>
                                </MaterialButton>
                            </Tooltip>
                            <Tooltip title="Rejeitar este time.">
                                <MaterialButton onClick={() => 
                                handleClickDisapprove(team.championship_id._id, team.team_id._id)} 
                                size="small">
                                    <CloseIconRed></CloseIconRed>
                                </MaterialButton>
                            </Tooltip>
                        </td>
                            </tr>
                        </Fragment>
                    )
                
            })
        } else {
            return <p>Não existem times pendentes de aprovação.</p>
        }

    }

    return (
        <>
            <div className='container'>
                <h1>Times Pendentes de Aprovação</h1>
                {loading ? 
                <CenterDiv>
                    <CircularProgress size={65}></CircularProgress>
                </CenterDiv> : 
                <Table responsive striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nome do Time</th>
                            <th>Campeonato</th>
                            <th>Status</th>
                            <th>Aprovar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {mountTeams()}
                    </tbody>
                </Table>}
            </div>
        </>
    )
}

export default TeamPendingList

const CheckIconGreen = styled(CheckIcon)`
    color: green;
`
const CloseIconRed = styled(CloseIcon)`
    color: red;
`