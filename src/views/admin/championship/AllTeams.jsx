import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { ListGroup } from 'react-bootstrap';
import { fetchGetAllTeams } from '../../../store/getAllTeams';
import { selectTeam } from '../../../store/myTeamDetailSelected';
import history from '../../../config/history';
import { CenterDiv } from '../../portal/ChampionshipList';
import { CircularProgress } from '@material-ui/core';

const AllTeams = () => {
    const dispatch = useDispatch()
    const [teams, setTeams] = useState([])
    const [loading, setLoading] = useState(true)
    useEffect(async () => {
        let data = await dispatch(fetchGetAllTeams())
        setTeams(data.payload)
        setLoading(false)
        return () => {};
    }, [])

    function navigateToMyTeamDetail(team){
        dispatch(selectTeam(team))
        history.push('/admin/teamsList/detail')
    }

    function mountTeamList(){
        if(teams.length > 0){
            return teams.map((team) => (
                <ListGroup.Item
                    key={team._id} onClick={() => navigateToMyTeamDetail
                    ({id: team._id, name: team.name})} className='item'>
                    {team.name}</ListGroup.Item>
            ))
        }else{
            return <p>Não existem times cadastrados!</p>
        }
        
    }

    return (
        <>
            <div class='container'>
                <h2 className='mt-3'>Times</h2>
                {loading ? 
                <CenterDiv>
                    <CircularProgress size={65}></CircularProgress>
                </CenterDiv> : 
                <ListGroupStyled className='mt-4 mb-4'>
                    {mountTeamList()}
                </ListGroupStyled> }
            </div >
        </>
    )
}

export default AllTeams

const ListGroupStyled = styled(ListGroup)`
    .item:hover{
        cursor: pointer;
        color: #0000EE;
    }
`
