import React, { useEffect, useState } from 'react'
import { Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { Fragment } from 'react';
import { selectChampionship } from '../../../store/championshipDetailSelected';
import { fetchChampionship } from '../../../store/championships';
import history from '../../../config/history';
import EditIcon from '@material-ui/icons/Edit';
import { selectChampionshipEdit } from '../../../store/championshipEdit';
import { CenterDiv } from '../../portal/ChampionshipList';
import { CircularProgress } from '@material-ui/core';


const ListarCampeonatos = () => {
  const state = useSelector(state => state.championshipReducer)
  const [championships, setChampionships] = useState([])
  const [loading, setLoading] = useState(true)
  const dispatch = useDispatch()
  useEffect(async () => {
    setLoading(true)
    let data = await dispatch(fetchChampionship())
    setChampionships(data.payload)
    setLoading(false)
  }, [])

  function navigateToChampionshipPage(payload) {
    dispatch(selectChampionship(payload))
    history.push('/admin/championshipList/detail')
  }

  function navigateToEditarCampeonatos(name, id) {
    dispatch(selectChampionshipEdit({ name: name, id: id }))
    history.push('championship')
  }

  let championshipType = {
    GROUP_STAGE: 'Fase de Grupos',
    PLAYOFFS: 'Mata-Mata',
    PLAYOFFSAndGROUP_STAGE: 'Fase de Grupos e Mata-Mata'
  }

  return (
    <>
      <div className='container'>
        <h2 className='mt-3'>Lista de Campeonatos</h2>
        {loading ? 
        <CenterDiv>
          <CircularProgress size={65}></CircularProgress>
        </CenterDiv> : 
        <Table responsive striped bordered hover>
          <thead>
            <tr>
              <th>Nome do Campeonato</th>
              <th>Tipo de Torneio</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {championships.length > 0 ? championships.map((championship) => (
              <Fragment>
                <TrStyled>
                  <td className='item' onClick={() => navigateToChampionshipPage
                    ({ id: championship._id, name: championship.name })}
                    key={championship._id} className='item'>
                    {championship.name}
                  </td>
                  <td>{championshipType[championship.phases]}
                  </td>
                  <td><EditIcon className='edit' onClick={() =>
                    navigateToEditarCampeonatos(championship.name, championship._id)}>
                  </EditIcon></td>
                </TrStyled>
              </Fragment>
            )) : <p>Não existem campeonatos cadastrados.</p>}
          </tbody>
        </Table>}
      </div >
    </>
  )
}

const TrStyled = styled.tr`
    .item:hover, .edit:hover{
        cursor: pointer;
        color: #0000EE;
    }
`
export default ListarCampeonatos