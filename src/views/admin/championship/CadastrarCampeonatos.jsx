import { useEffect, useState } from 'react';
import { Form, Button, Alert, Spinner } from 'react-bootstrap';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { fetchChampionshipPost } from '../../../store/postChampionship';
import history from '../../../config/history';
import { fetchChampionshipEdit } from '../../../store/editChampionship';
import { Radio } from '@material-ui/core';
import { FormControlLabel } from '@material-ui/core';
import { RadioGroup } from '@material-ui/core';

const CadastrarCampeonatos = () => {
    const [nome, setNome] = useState('')
    const [radioValue, setRadioValue] = useState('GROUP_STAGE')
    const championshipEdit = useSelector(state => state.championshipEdit)
    const [editing, setEditing] = useState(false)
    const dispatch = useDispatch()

    useEffect(() => {
        if(championshipEdit.name){
          setEditing(true)
          setNome(championshipEdit.name)
        }
        return () => {};
      }, [])

    function formIsValid() {
        return nome;
    }

    async function createChampionship() {
        const data = await dispatch(
            fetchChampionshipPost({phases: radioValue, name: nome}))
        history.push('championshipList')
    }
    async function editChampionship(){
        const data = await dispatch(
            fetchChampionshipEdit({phases: radioValue, name: nome, championshipId: championshipEdit.id}))
        history.push('championshipList')
    }

    return (
        <>
            <DivForm>
                <div className='container'>
                    <Form.Group controlId="formBasic">
                        <Form.Label>Nome do campeonato</Form.Label>
                        <Form.Control type="email" placeholder="" value={nome}
                            onChange={(event) => setNome(event.target.value)} />
                    </Form.Group>
                    <span>Tipo do Campeonato</span>
                    <RadioGroup aria-label="Tipo do Campeonato" name="tipo" value={radioValue}
                        onChange={(event) => setRadioValue(event.target.value)}>
                        <FormControlLabel id='first' value="GROUP_STAGE" control={<Radio />}
                            label="Fase de grupos" />
                        <FormControlLabel value="PLAYOFFS" control={<Radio />}
                            label="Mata-Mata" />
                        <FormControlLabel value="PLAYOFFSAndGROUP_STAGE"
                            control={<Radio />} label="Fase de grupos e mata-mata" />
                    </RadioGroup>

                    <Button disabled={!formIsValid()} onClick={editing ? editChampionship : createChampionship} variant="primary" type="button">
                        {editing ? 'Editar' : 'Criar'}
                        </Button>

                </div>
            </DivForm>
        </>
    )
}

const DivForm = styled.div`
padding-top: 3rem; 
#first{
    margin-top: 0.4rem;
}
    .form{
        padding: 2rem;
        max-width: 600px;
    }
    .spinner{
        margin-left: 1rem;
    }
`
export default CadastrarCampeonatos
