import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { ListGroup, Table } from 'react-bootstrap';
import styled from 'styled-components';
import { fetchChampionshipById } from '../../../store/getChampionshipById';
import { Fragment } from 'react';
import history from '../../../config/history';
import { selectTeam } from '../../../store/myTeamDetailSelected';
import { Style } from '@material-ui/icons';
import { CenterDiv } from '../../portal/ChampionshipList';
import { CircularProgress } from '@material-ui/core';
const ChampionshipTable = () => {
    const championship = useSelector(state => state.championshipDetailSelected)
    const dispatch = useDispatch()
    const [teams, setTeams] = useState([])
    const [loading, setLoading] = useState(true)
    useEffect(async () => {
        const data = await dispatch(fetchChampionshipById(championship.id))
        const arrTeam = []
        for (const team of data.payload.teams) {
            if(team.team_id){
                arrTeam.push(team)
            }
        }
        setTeams(arrTeam)
        setLoading(false)
        return () => {
        };
    }, [])

    function navigateToTeamPage(name, id){
        dispatch(selectTeam({name, id}))
        history.push('/admin/teamsList/detail')
    }

    function mountTeams(){
        
        if(teams.length > 0){
            return teams.map((team) => {
                    return (
                        <Fragment>
                        <TrStyled>
                        <td className='item' onClick={() => navigateToTeamPage(team.team_id.name, team.team_id._id)}
                            key={team.team_id._id} className='item'>
                            {team.team_id.name}
                        </td>
                        <td>{statusChampionship[team.status]}
                        </td>
                        </TrStyled>
                        </Fragment>
                    )
                
            })
        }else{
            return <p>Não existem times cadastrados para este campeonato.</p>
        }
        
    }

    return (
        <>
            <div className='container'>
            <StyledH2 className='mt-3'>{championship.name}</StyledH2>
                <h3 className='mt-5'>Times Participantes</h3>
                {loading ? 
                <CenterDiv>
                    <CircularProgress size={65}></CircularProgress>
                </CenterDiv> : 
                <Table responsive striped bordered hover>
                    <thead>
                        <tr>
                            <th>Nome do Time</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {mountTeams()}
                    </tbody>
                </Table>}
            </div >
        </>
    )
}

export default ChampionshipTable

const ListGroupStyled = styled(ListGroup)`
    
`
const TrStyled = styled.tr`
    .item:hover{
        cursor: pointer;
        color: #0000EE;
    }
`

const StyledH2 = styled.h2`
    overflow-wrap: break-word;
`


export const statusChampionship = {
    PENDING: 'Pendente',
    APPROVED: 'Confirmado',
    DISAPPROVED: 'Rejeitado'
}