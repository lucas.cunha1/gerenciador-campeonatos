import {
  Redirect,
  Route
} from "react-router-dom";
import history from '../../config/history';
import { isAuthenticated, getUser } from '../../config/auth';
import CadastrarCampeonatos from './championship/CadastrarCampeonatos';
import ListarCampeonatos from './championship/ListarCampeonatos';
import HomeAdmin from './championship/HomeAdmin';
import AllTeams from './championship/AllTeams';
import TeamDetail from './championship/TeamDetail';
import Team from '../portal/Team';
import ChampionshipTable from './championship/ChampionshipTable';
import { useSelector } from 'react-redux';
import TeamPendingList from './championship/TeamPendingList';
const Admin = (props) => {
  const loginState = useSelector((state) => state.user)
  const AdminRoute = ({...rest}) => {
    const user = getUser()
    if(loginState.isAdmin !== true && history.location.pathname !== '/login' 
    && history.location.pathname !== '/cadastrar'){
      return <Redirect push to="/login" />
    }
    if(user.is_admin !== true){
      return <Redirect push to="/login" />
    }
    return <Route {...rest}></Route>
    
  }

  return (
    <>
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/championship'} component={CadastrarCampeonatos} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/championshipList'} component={ListarCampeonatos} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/championshipList/detail'} component={ChampionshipTable} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/home'} component={() => <h1>Homer 2</h1>} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/homeAdmin'} component={HomeAdmin} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/teamsList'} component={AllTeams} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/teamsList/detail'} component={TeamDetail} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/teamsList/team/edit'} component={() => <Team edit={true}/>} />
      <AdminRoute exact basename={props.match.path} path={props.match.path + '/teamsPending'} component={TeamPendingList}/>
    </>
  )
}

export default Admin
