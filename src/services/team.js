import http from '../config/config';

const listarTodosOsTimes = () => http.get('/teams')
const listarTime = (teamId) => http.get(`/teams/${teamId}`)
const criarTime = (payload) => http.post('/teams', {name: payload.nome, user_id: payload.user_id})
const editarTime = (payload) => http.patch(`/teams/${payload.teamId}`, 
{championship_id: payload.championshipId})

const EditarStatusTime = (payload) => http.patch(`/teams/status/${payload.teamId}`,
{championship_id: payload.championshipId, status: payload.status}) 

const ListarTimesPendentes = () => http.get('teams/status/pending')

export {listarTime, listarTodosOsTimes, criarTime, editarTime, EditarStatusTime, ListarTimesPendentes}