import http from '../config/config';

const listarCampeonatos = () => http.get('/championship')
const listarCampeonato = (champinshipId) => http.get(`/championship/${champinshipId}`)
const criarCampeonato = (payload) => http.post('/championship', {name: payload.name, phases:payload.phases})
const editarCampeonato = (payload) => http.patch(`/championship/${payload.championshipId}`, {name: payload.name, phases:payload.phases})


export {listarCampeonatos, listarCampeonato, criarCampeonato, editarCampeonato}