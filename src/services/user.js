import http from '../config/config';

const login = (email, senha) => http.post('/auth',{email: email, password: senha})
const cadastro = ({name, email, senha, isAdmin=false}) => http.post('/user',{name: name,email: email, password: senha, is_admin: isAdmin})
const GetUser = (id) => http.get(`user/${id}`)

export {login, cadastro, GetUser}