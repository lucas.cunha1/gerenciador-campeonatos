import { listarCampeonato } from '../services/championships';
import createAsyncSlice from './helper/createAsyncSlice';

const getChampionshipById = createAsyncSlice({
    name: 'getChampionshipById',
    service: listarCampeonato
});

export const fetchChampionshipById = getChampionshipById.asyncAction;

export default getChampionshipById.reducer