import createAsyncSlice from './helper/createAsyncSlice';
import { editarCampeonato } from '../services/championships';

const editChampionship = createAsyncSlice({
    name: 'editChampionship',
    service: editarCampeonato
});

export const fetchChampionshipEdit = editChampionship.asyncAction;

export default editChampionship.reducer