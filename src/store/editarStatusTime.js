import createAsyncSlice from './helper/createAsyncSlice';
import { EditarStatusTime } from '../services/team';

const editarStatusTime = createAsyncSlice({
    name: 'editarStatusTime',
    service: EditarStatusTime
});

export const fetchEditarStatusTime = editarStatusTime.asyncAction;

export default editarStatusTime.reducer