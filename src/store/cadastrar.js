import createAsyncSlice from './helper/createAsyncSlice';
import { cadastro } from '../services/user';

const cadastrar = createAsyncSlice({
    name: 'cadastrar [GET]',
    service: cadastro
});

export const fetchCadastrar = cadastrar.asyncAction;

export default cadastrar.reducer