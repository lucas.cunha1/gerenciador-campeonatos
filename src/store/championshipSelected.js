const { createSlice } = require('@reduxjs/toolkit');

const championshipSelected = createSlice({
    name: 'championshipSelected',
    initialState: null,
    reducers:{
        selectChampionship(state, action){
            return action.payload
        }
    }
})

export const { selectChampionship } = championshipSelected.actions;
export default championshipSelected.reducer;
