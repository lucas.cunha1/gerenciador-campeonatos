const { createSlice } = require('@reduxjs/toolkit');

const championshipDetailSelected = createSlice({
    name: 'championshipDetailSelected',
    initialState: {
        name: null,
        id: null
    },
    reducers:{
        selectChampionship(state, action){
            state.name = action.payload.name
            state.id = action.payload.id
        }
    }
})

export const { selectChampionship } = championshipDetailSelected.actions;
export default championshipDetailSelected.reducer;
