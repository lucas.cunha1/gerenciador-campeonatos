import { criarTime, editarTime } from '../services/team';
import createAsyncSlice from './helper/createAsyncSlice';
import { combineReducers } from 'redux';

const createTeam = createAsyncSlice({
    name: 'team',
    service: criarTime
});

const patchTeam = createAsyncSlice({
    name: 'team',
    service: editarTime,
    initialState:{
        loaded: false
    },
    reducers:{
        loaded(state) {
            if(state.data != null){
                state.loaded = true
            }
        },
        resetLoaded(state){
            state.loaded = false
        }
    }
});

export const {loaded, resetLoaded} = patchTeam.actions

const fetchCreateTeam = createTeam.asyncAction;
export const fetchPatchTeam = patchTeam.asyncAction;

export default combineReducers({createTeam: createTeam.reducer, 
    editTeamAddChampionship: patchTeam.reducer})

export const createTeamAndAddChampionship = (externalPayload) => async (dispatch) => {
    try {
        let response
        const {payload} = await dispatch(fetchCreateTeam(
            {nome: externalPayload.nome, user_id: externalPayload.user_id}))
        if(payload){
            console.log(externalPayload)
            response = await dispatch(fetchPatchTeam({teamId: payload._id, 
                championshipId: externalPayload.championshipId}))
            dispatch(loaded())
        }
        return response
    } catch (error) {
        console.log(error)
    }
}