import createAsyncSlice from './helper/createAsyncSlice';
import { GetUser } from '../services/user';

const getUser = createAsyncSlice({
    name: 'championship',
    service: GetUser
});

export const fetchUser = getUser.asyncAction;

export default getUser.reducer