const { createSlice } = require('@reduxjs/toolkit');

const myTeamDetailSelected = createSlice({
    name: 'myTeamDetailSelected',
    initialState: {
        name: null,
        id: null
    },
    reducers:{
        selectTeam(state, action){
            console.log(action)
            state.name = action.payload.name;
            state.id = action.payload.id;
        }
    }
})

export const { selectTeam } = myTeamDetailSelected.actions;
export default myTeamDetailSelected.reducer;
