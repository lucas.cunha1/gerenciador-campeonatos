import createAsyncSlice from './helper/createAsyncSlice';
import { criarCampeonato } from '../services/championships';

const postChampionship = createAsyncSlice({
    name: 'postChampionship',
    service: criarCampeonato
});

export const fetchChampionshipPost = postChampionship.asyncAction;

export default postChampionship.reducer