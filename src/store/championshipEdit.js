const { createSlice } = require('@reduxjs/toolkit');

const championshipEdit = createSlice({
    name: 'championshipEdit',
    initialState: {
        name: null,
        id: null
    },
    reducers:{
        selectChampionshipEdit(state, action){
            state.name = action.payload.name
            state.id = action.payload.id
        }
    }
})

export const { selectChampionshipEdit } = championshipEdit.actions;
export default championshipEdit.reducer;
