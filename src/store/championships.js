import { listarCampeonatos } from '../services/championships';
import createAsyncSlice from './helper/createAsyncSlice';

const championship = createAsyncSlice({
    name: 'championship [GET]',
    service: listarCampeonatos
});

export const fetchChampionship = championship.asyncAction;

export default championship.reducer