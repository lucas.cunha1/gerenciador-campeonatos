import { listarTime } from '../services/team';
import createAsyncSlice from './helper/createAsyncSlice';

const getTeamById = createAsyncSlice({
    name: 'getTeamById',
    service: listarTime
});

export const fetchTeamById = getTeamById.asyncAction;

export default getTeamById.reducer