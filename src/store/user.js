const { createSlice } = require('@reduxjs/toolkit');

const user = createSlice({
    name: 'user',
    initialState: {
        userId: null,
        isAdmin: null,
        name: null
    },
    reducers:{
        logoutStore(state){
            state.userId = null
            state.isAdmin = null
            state.name = null
        },
        loginStore(state, action){
            state.isAdmin = action.payload.is_admin
            state.userId = action.payload.id
            state.name = action.payload.name
        }
    }
})

export const { loginStore, logoutStore } = user.actions;
export default user.reducer;
