import championshipReducer from './championships';
import championshipSelected from './championshipSelected';
import createTeam from './teamPost';
import user from './user';
import championshipDetailSelected from './championshipDetailSelected';
import getUser from './getUser';
import myTeamDetailSelected from './myTeamDetailSelected';
import championshipEdit from './championshipEdit';
import {
    combineReducers,
    configureStore,
    getDefaultMiddleware,
  } from '@reduxjs/toolkit';
  
  const middleware = [...getDefaultMiddleware()];
  
  const reducer = combineReducers({ championshipReducer, championshipSelected, 
    championshipDetailSelected, createTeam, user, getUser, myTeamDetailSelected, championshipEdit });
  const store = configureStore({ reducer : reducer });
  
  export default store;
  