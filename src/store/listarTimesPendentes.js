import { ListarTimesPendentes } from '../services/team';
import createAsyncSlice from './helper/createAsyncSlice';

const listarTimesPendentes = createAsyncSlice({
    name: 'listarTimesPendentes',
    service: ListarTimesPendentes
});

export const fetchListarTimesPendentes = listarTimesPendentes.asyncAction;

export default listarTimesPendentes.reducer