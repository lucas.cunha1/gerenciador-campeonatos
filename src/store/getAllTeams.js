import { listarCampeonato } from '../services/championships';
import createAsyncSlice from './helper/createAsyncSlice';
import { listarTodosOsTimes } from '../services/team';

const getAllTeams = createAsyncSlice({
    name: 'getAllTeams',
    service: listarTodosOsTimes
});

export const fetchGetAllTeams = getAllTeams.asyncAction;

export default getAllTeams.reducer